import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget profileView = Container(
    child: Row(
      children: [
        Expanded(
            flex: 1,
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Container(
                  child: Image.asset(
                'image/61160151.jfif',
                width: 130,
                height: 200,
                fit: BoxFit.cover,
              ))
            ])),
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("ชื่อ: ปิยณัฐ นุ่นมา", style: TextStyle(fontSize: 22)),
            Text("ชื่อเล่น: ต๊ะ", style: TextStyle(fontSize: 22)),
            Text("เพศ: ชาย", style: TextStyle(fontSize: 22)),
            Text("อายุ: 22", style: TextStyle(fontSize: 22)),
          ],
        ))
      ],
    ),
  );

  Widget SkillView = Container(
      child: Card(
          child: Column(children: [
    ListTile(
        title: const Text(
          'ภาษา',
          style: TextStyle(fontSize: 25),
        )),
    Padding(
      padding: const EdgeInsets.all(16.0),
      child: Wrap(
        direction: Axis.horizontal,
        runSpacing: 20.0,
        children: [
          Image.network(
            'http://4.bp.blogspot.com/-E9aD4jovFrY/V_-l2hMA8wI/AAAAAAAAAAo/eJzxJHcHeXga91z9YstKAqY397NKhmvEwCK4B/s1600/2.jpg',
            width: 150,
            height: 150,
          ),
          Image.network(
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTD9BwaN-pzQEjc2e-AOPz0YUOssMierVD8IA&usqp=CAU',
          )
        ],
      ),
    ),
  ])));

  Widget education = Container(
      child: Card(
          child: Column(children: [
    ListTile(
        title: const Text(
          'ประวัติการศึกษา',
          style: TextStyle(fontSize: 25),
        )),
    Padding(
      padding: const EdgeInsets.all(16.0),
      child: Text(
        '2015-2018  ระดับมัธยมศึกษา โรงเรียนศรีสะเกษวิทยาลัย \n'
        '2018-ปัจจุบัน  ระดับอุดมศึกษา มหาวิทยาลัยบูรพาคณะวิทยาการสารสนเทศ สาขาวิทยาการคอมพิวเตอร์',
        style: TextStyle(fontSize: 20),
      ),
    )
  ])));

  Widget personalrecord = Container(
      child: Card(
          child: Column(children: [
    ListTile(
        title: const Text(
          'ประวัติส่วนตัว',
          style: TextStyle(fontSize: 25),
        )),
    Padding(
      padding: const EdgeInsets.all(16.0),
      child: Text(
        'ที่อยู่ : 229 ม.8 ถ.กสิกรรม อ.เมือง จ.ศรีสะเกษ 33000 \n'
        'อีเมล : 61160151@go.buu.ac.th \n'
        'วันเกิด : 21 มกราคม 2542',
        style: TextStyle(fontSize: 20),
      ),
    )
  ])));

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My resume',
      home: Scaffold(
        appBar: AppBar(
          title: Text('My resume'),
        ),
        body: ListView(
          children: [profileView, SkillView, education,personalrecord],
        ),
      ),
    );
  }
}
